﻿using System;
using System.Collections.Generic;

namespace NoteBook
{
    class NoteBook
    {
        static void Main(string[] args)
        {
            startWorking();
        }
        static List<Note> notes = new List<Note>();
        static void startWorking()
        {
            Console.WriteLine("\nВыберите Действие:");
            Console.WriteLine("1 - Добавить запись");
            Console.WriteLine("2 - Редактировать существующую запись");
            Console.WriteLine("3 - Удалить существующую запись");
            Console.WriteLine("4 - Просмотреть существующую запись");
            Console.WriteLine("5 - Просмотреть все существующие записи");
            Console.WriteLine("6 - Выйти");
            switch (Console.ReadKey().KeyChar)
            {
                case '1':
                    notes.Add(createNote());
                    startWorking();
                    break;
                case '2':
                    if (notes.Count == 0)
                    {
                        Console.WriteLine("\nНет доступных для редактирования записей");
                        startWorking();
                        break;
                    }
                    changeNote(choseNote());
                    break;
                case '3':
                    if (notes.Count == 0)
                    {
                        Console.WriteLine("\nНет доступных для удаления записей");
                        startWorking();
                        break;
                    }
                    deleteNote(choseNote());
                    startWorking();
                    break;
                case '4':
                    if (notes.Count == 0)
                    {
                        Console.WriteLine("\nНет доступных для удаления записей");
                        startWorking();
                        break;
                    }
                    showNote(choseNote());
                    startWorking();
                    break;
                case '5':
                    foreach (Note note in NoteBook.notes)
                    {
                        Console.WriteLine(note);
                    }
                    startWorking();
                    break;
                case '6':
                    break;
                default:
                    Console.WriteLine("\nОшибка Ввода:");
                    startWorking();
                    break;
            }
        }
        static Note choseNote()
        {
            Console.WriteLine("\nВыберите запись:");
            foreach (Note note in NoteBook.notes)
            {
                Console.WriteLine("Что-бы выбрать запись №{0} нажмите {1}", notes.IndexOf(note), notes.IndexOf(note) + 1);
            }
            char c = Console.ReadKey().KeyChar;
            bool test = int.TryParse(c.ToString(), out int res);
            if ((res>=1) && (res<=notes.Count) && (test = true))
            {
                return notes[res-1];
            }
            else
            {
                Console.WriteLine("Ошибка ввода:");
                return choseNote();
            }
        }
        static Note createNote()
        {
            Note newNote = new Note();
            newNote.Name = writeName();
            newNote.Surname = writeSurname();
            newNote.Country = writeCountry();
            newNote.PhoneNumber = writePhoneNumber();
            newNote.MiddleName = writeMiddleName();
            newNote.Organization = writeOrganization();
            newNote.Position = writePosition();
            newNote.DateOfBirth = writeDateOfBirth();
            newNote.AnotherInfo = writeAnotherInfo();
            return newNote;
        }
        static void changeNote(Note note)
        {
            Console.WriteLine("\nЧто вы хотите поменять?");
            Console.WriteLine("Что-бы поменять имя нажмите - 1\t\t" + note.Name);
            Console.WriteLine("Что-бы поменять фамилию нажмите - 2\t\t" + note.Surname);
            Console.WriteLine("Что-бы поменять отчество нажмите - 3\t\t" + note.MiddleName);
            Console.WriteLine("Что-бы поменять телефон нажмите - 4\t\t" + note.PhoneNumber);
            Console.WriteLine("Что-бы поменять организацию нажмите - 5\t\t" + note.Organization);
            Console.WriteLine("Что-бы поменять должность нажмите - 6\t\t" + note.Position);
            Console.WriteLine("Что-бы поменять дату рождения нажмите - 7\t\t" + note.DateOfBirth);
            Console.WriteLine("Что-бы поменять другую информацию нажмите - 8\t\t" + note.AnotherInfo);
            Console.WriteLine("Что-бы ничего не менять нажмите -9");
            switch (Console.ReadKey().KeyChar)
            {
                case '1':
                    note.Name = writeName();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    int k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '2':
                    note.Surname = writeSurname();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '3':
                    note.MiddleName = writeMiddleName();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '4':
                    note.PhoneNumber = writePhoneNumber();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '5':
                    note.Organization = writeOrganization();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '6':
                    note.Position = writePosition();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '7':
                    note.DateOfBirth = writeDateOfBirth();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '8':
                    note.AnotherInfo = writeAnotherInfo();
                    Console.WriteLine("\nХотите продолжить редактировать запись? (y/n)");
                    k = 0;
                    while (k != 1)
                    {
                        switch (Console.ReadKey().KeyChar)
                        {
                            case 'y':
                                changeNote(note);
                                k = 1;
                                break;
                            case 'n':
                                startWorking();
                                k = 1;
                                break;
                            default:
                                Console.WriteLine("Неверный ввод");
                                break;

                        }
                    }
                    break;
                case '9':
                    startWorking();
                    break;
                default:
                    Console.WriteLine("\nОшибка Ввода:");
                    changeNote(note);
                    break;
            }

        }
        static void deleteNote(Note note)
        {
            Console.WriteLine("\nУверены что хотите удалить запись? (y/n)");
            int k = 0;
            while (k != 1)
            {
                switch (Console.ReadKey().KeyChar)
                {
                    case 'y':
                        notes.Remove(note);
                        k = 1;
                        break;
                    case 'n':
                        k = 1;
                        break;
                    default:
                        Console.WriteLine("\nНеверный ввод");
                        break;

                }
            }
        }
        static void showNote(Note note)
        {
            Console.WriteLine("\nИмя - " + note.Name);
            Console.WriteLine("Фамилия - " + note.Surname);
            Console.WriteLine("Отчество - " + note.MiddleName);
            Console.WriteLine("Телефон - " + note.PhoneNumber);
            Console.WriteLine("Организация - " + note.Organization);
            Console.WriteLine("Должность - " + note.Position);
            Console.WriteLine("Дата рождения - " + note.DateOfBirth);
            Console.WriteLine("Другая информация - " + note.AnotherInfo);
            Console.WriteLine("Нажмите любую кнопку что-бы продолжить");
            Console.ReadKey();
        }
        static string writeName()
        {
            string currentProperty = "";
            while (currentProperty == "")
            {
                Console.WriteLine("\nВведите Имя:");
                currentProperty = Console.ReadLine();
            }
            return currentProperty;

        }
        static string writeSurname()
        {
            string currentProperty = "";
            while (currentProperty == "")
            {
                Console.WriteLine("\nВведите Фамилию:");
                currentProperty = Console.ReadLine();
            }
            return currentProperty;
        }
        static string writeCountry()
        {
            string currentProperty = "";
            while (currentProperty == "")
            {
                Console.WriteLine("\nВведите Страну Проживания:");
                currentProperty = Console.ReadLine();
            }
            return currentProperty;
        }
        static string writeMiddleName()
        {
            string currentProperty = ""; 
            Console.WriteLine("\nВведите Отчество:");
            currentProperty = Console.ReadLine();
            return currentProperty;
        }
        static string writeOrganization()
        {
            string currentProperty = "";
            Console.WriteLine("\nВведите Организацию:");
            currentProperty = Console.ReadLine();
            return currentProperty;
        }
        static string writePosition()
        {
            string currentProperty = "";
            Console.WriteLine("\nВведите Должность:");
            currentProperty = Console.ReadLine();
            return currentProperty;
        }
        static string writeAnotherInfo()
        {
            string currentProperty = "";
            Console.WriteLine("\nВведите Иную информацию:");
            currentProperty = Console.ReadLine();
            return currentProperty;
        }
        static string writePhoneNumber()
        {
            string number;
            while (true)
            {
                Console.WriteLine("\nВведите Номер (10 цифр)");
                number = "+7";
                Console.Write(number);
                string ch = (string)Console.ReadLine();
                if (ch.Length == 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        char c = ch.ToCharArray()[i];
                        bool test = int.TryParse(c.ToString(), out int res);
                        if ((res >= 0) && (res < 10) && (test))
                        {
                            number = number.Insert(number.Length, c.ToString());
                        }
                    }
                }
                if (number.Length == 12)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Неверный ввод");
                }

            }
            return number;
        }
        static string writeDateOfBirth()
        {
            string date;
            Console.WriteLine("\nВведите День Рождения (dd)");
            date = "";
            string ch = (string)Console.ReadLine();
            if (ch.Length == 2)
            {
                bool test = int.TryParse(ch, out int res);
                if ((res > 0) && (res <= 31) && (test))
                {
                    date = date.Insert(date.Length, ch);
                    date = date.Insert(date.Length, ":");
                }
            }
            Console.WriteLine("Введите Месяц Рождения (mm)");
            ch = (string)Console.ReadLine(); 
            if (ch.Length == 2) {
                bool test = int.TryParse(ch, out int res);
                if ((res > 0) && (res <= 12) && (test))
                {
                    date = date.Insert(date.Length, ch);
                    date = date.Insert(date.Length, ":");
                }
            }
            Console.WriteLine("Введите Год Рождения (yyyy)");
            ch = (string)Console.ReadLine();
            if (ch.Length == 4)
            {
                bool test = int.TryParse(ch, out int res);
                if ((res > 1880) && (res <= 2019) && (test))
                {
                    date = date.Insert(date.Length, ch);
                }
            }
            if (date.Length != 10)
            {
                date = " ";
            }
            return date;
        }
    }
    class Note
    {
        private Dictionary<string, string> info = new Dictionary<string, string>(9);
        public string Name
        {
            get => info["Name"];
            set => info["Name"] = value;
        }
        public string Surname
        {
            get => info["Surname"];
            set => info["Surname"] = value;
        }
        public string Country
        {
            get => info["Country"];
            set => info["Country"] = value;
        }
        public string PhoneNumber
        {
            get => info["Phone Number"];
            set => info["Phone Number"] = value;
        }
        public string MiddleName
        {
            get
            {
                if (info.TryGetValue("Otchestvo", out string kek))
                {
                    return kek;
                }
                return "-";
            }
            set => info["Otchestvo"] = value;
        }
        public string Organization
        {
            get
            {
                if (info.TryGetValue("Organization", out string kek))
                {
                    return kek;
                }
                return "-";
            }
            set => info["Organization"] = value;
        }
        public string Position
        {
            get
            {
                if (info.TryGetValue("Doljnost", out string kek))
                {
                    return kek;
                }
                return "-";
            }
            set => info["Doljnost"] = value;
        }
        public string AnotherInfo
        {
            get
            {
                if (info.TryGetValue("Another Info", out string kek))
                {
                    return kek;
                }
                return "-";
            }
            set => info["Another Info"] = value;
        }
        public string DateOfBirth
        {
            get
            {
                if (info.TryGetValue("Date of birth", out string kek))
                {
                    return kek;
                }
                return "-";
            }
            set => info["Date of birth"] = value;
        }

        public override string ToString()
        {
            return ("\n" + this.Name + " " + this.Surname + " " + this.PhoneNumber);
        }



    }
}
